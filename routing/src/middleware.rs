use axum::{
    http::{Request, StatusCode},
    middleware::Next,
    response::Response,
};

use crate::routes::HeaderMessage;

pub async fn extract_header_message<T>(
    mut req: Request<T>,
    next: Next<T>,
) -> Result<Response, StatusCode> {
    let message_header = req.headers().get("message");

    if let Some(msg) = message_header {
        let msg = msg
            .to_str()
            .map_err(|_| StatusCode::BAD_REQUEST)?
            .to_owned();

        req.extensions_mut().insert(HeaderMessage(msg));
    } else {
        req.extensions_mut().insert(HeaderMessage("".to_string()));
    }

    Ok(next.run(req).await)
}
