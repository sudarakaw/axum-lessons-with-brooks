mod middleware;
mod routes;

use routes::create_routes;

pub async fn run() {
    let app = create_routes();

    axum::Server::bind(&"127.0.0.1:5000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap()
}
