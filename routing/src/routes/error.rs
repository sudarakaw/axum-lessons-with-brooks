use axum::http::StatusCode;

pub async fn handler() -> Result<(), StatusCode> {
    Err(StatusCode::IM_A_TEAPOT)
}
