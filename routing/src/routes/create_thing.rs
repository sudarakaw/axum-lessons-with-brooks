use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
};

pub async fn handler() -> Response {
    (StatusCode::CREATED, "Thing created!!!".to_string()).into_response()
}
