mod create_thing;
mod custom_middleware;
mod echo_json;
mod echo_string;
mod error;
mod get_json;
mod headers;
mod message;
mod path_variables;
mod query_parameter;
mod root;

use axum::{
    extract::FromRef,
    http::Method,
    middleware,
    routing::{get, post},
    Router,
};
use tower_http::cors::{Any, CorsLayer};

use crate::middleware::extract_header_message;

pub use custom_middleware::HeaderMessage;

#[derive(Clone, FromRef)]
pub struct SharedData {
    message: String,
}

pub fn create_routes() -> Router {
    let cors = CorsLayer::new()
        .allow_methods([Method::GET, Method::POST])
        .allow_origin(Any);

    let shared_data = SharedData {
        message: "Hello from shared data, I am a State now".to_string(),
    };

    Router::new()
        .route("/", get(root::handler))
        .route("/echo-string", post(echo_string::handler))
        .route("/echo-json", post(echo_json::handler))
        .route("/things/:id", get(path_variables::handler))
        .route("/things/42", get(path_variables::handler_42))
        .route("/query", get(query_parameter::handler))
        .route("/whoami", get(headers::handler_typed))
        .route("/custom-header", get(headers::handler_untyped))
        .route("/message", get(message::handler))
        .route("/error", get(error::handler))
        .route("/create-thing", post(create_thing::handler))
        .route("/give-me-json", get(get_json::handler))
        .layer(cors)
        .with_state(shared_data)
        .route("/custom-middleware", get(custom_middleware::handler))
        .layer(middleware::from_fn(extract_header_message))
}
