use axum::{headers::UserAgent, http::HeaderMap, TypedHeader};

pub async fn handler_typed(TypedHeader(user_agent): TypedHeader<UserAgent>) -> String {
    user_agent.to_string()
}

pub async fn handler_untyped(headers: HeaderMap) -> String {
    let message = headers
        .get("X-Message")
        .unwrap()
        .to_str()
        .unwrap()
        .to_owned();

    let nox = headers.get("noxhere").unwrap().to_str().unwrap().to_owned();

    format!("{message}\n{nox}")
}
