use axum::Json;
use serde::Serialize;

#[derive(Serialize)]
pub struct Data {
    message: String,
    account: u32,
    username: String,
}

pub async fn handler() -> Json<Data> {
    Json(Data {
        message: "Sample JSON data".to_string(),
        account: 42,
        username: "suda".to_string(),
    })
}
