use axum::extract::Path;

pub async fn handler(Path(id): Path<u32>) -> String {
    dbg!(&id);

    format!("Thing ID: {}", id)
}

pub async fn handler_42() -> String {
    "Nope!!!".to_string()
}
