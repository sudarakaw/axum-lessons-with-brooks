use axum::{extract::Query, Json};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct QueryData {
    message: Option<String>,
    id: Option<u32>,
}

pub async fn handler(Query(data): Query<QueryData>) -> Json<QueryData> {
    dbg!(&data);

    Json(data)
}
