use axum::Extension;

#[derive(Debug, Clone)]
pub struct HeaderMessage(pub String);

pub async fn handler(Extension(message): Extension<HeaderMessage>) -> String {
    dbg!(&message);

    message.0
}
