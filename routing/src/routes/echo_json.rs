use axum::Json;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize)]
pub struct EchoMessage {
    message: String,
}

#[derive(Serialize)]
pub struct EchoResponse {
    message: String,
    server_status: String,
}

pub async fn handler(Json(body): Json<EchoMessage>) -> Json<EchoResponse> {
    dbg!(&body);

    Json(EchoResponse {
        message: body.message,
        server_status: "READY!!!".to_string(),
    })
}
