use axum::{routing, Router};

async fn root() -> String {
    "Worked!!!!".to_owned()
}

#[tokio::main]
async fn main() {
    let app = Router::new().route("/", routing::get(root));

    axum::Server::bind(&"127.0.0.1:5000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap()
}
