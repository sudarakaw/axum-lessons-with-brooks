use std::env;

use axum::http::StatusCode;
use chrono::{Duration, Utc};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};

use super::error::AppError;

#[derive(Serialize, Deserialize)]
pub struct Claims {
    exp: usize,
    iat: usize,
}

pub fn create() -> Result<String, StatusCode> {
    let secret = env::var("JWT_SECRET").unwrap_or_default();

    let mut now = Utc::now();
    let iat = now.timestamp() as usize;

    now += Duration::seconds(30);

    let exp = now.timestamp() as usize;

    let claims = Claims { iat, exp };

    encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(secret.as_bytes()),
    )
    .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)
}

pub fn is_valid(token: &str) -> Result<(), AppError> {
    let secret = env::var("JWT_SECRET").unwrap_or_default();

    decode::<Claims>(
        token,
        &DecodingKey::from_secret(secret.as_bytes()),
        &Validation::default(),
    )
    .map_err(|error| match error.kind() {
        jsonwebtoken::errors::ErrorKind::ExpiredSignature => AppError::new(
            StatusCode::UNAUTHORIZED,
            "Your session has expired, please login again.",
        ),
        _ => AppError::new(StatusCode::INTERNAL_SERVER_ERROR, "Internal server error"),
    })?;

    Ok(())
}
