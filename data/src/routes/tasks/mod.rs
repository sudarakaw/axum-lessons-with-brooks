mod delete;
mod partial_update;

pub use delete::delete;
pub use partial_update::partial_update;

use axum::{
    extract::{Path, Query, State},
    http::StatusCode,
    response::IntoResponse,
    Extension, Json,
};
use sea_orm::{
    prelude::DateTimeWithTimeZone, ActiveModelTrait, ColumnTrait, Condition, DatabaseConnection,
    EntityTrait, QueryFilter, Set,
};
use serde::{Deserialize, Serialize};

use crate::database::users;
use crate::database::{prelude::Tasks, tasks};

#[derive(Deserialize)]
pub struct RequestTask {
    title: String,
    description: Option<String>,
    priority: Option<String>,
}

pub async fn create(
    State(db): State<DatabaseConnection>,
    Extension(user): Extension<users::Model>,
    Json(request_task): Json<RequestTask>,
) -> Result<impl IntoResponse, StatusCode> {
    let new_task = tasks::ActiveModel {
        priority: Set(request_task.priority),
        title: Set(request_task.title),
        description: Set(request_task.description),
        user_id: Set(Some(user.id)),
        ..Default::default()
    };

    new_task.save(&db).await.unwrap();

    Ok(StatusCode::CREATED)
}

#[derive(Serialize)]
pub struct ResponseTask {
    id: i32,
    title: String,
    priority: Option<String>,
    description: Option<String>,
    deleted_at: Option<DateTimeWithTimeZone>,
    user_id: Option<i32>,
}

pub async fn get_one(
    State(db): State<DatabaseConnection>,
    Path(id): Path<i32>,
) -> Result<Json<ResponseTask>, StatusCode> {
    let result = Tasks::find_by_id(id)
        .filter(tasks::Column::DeletedAt.is_null())
        .one(&db)
        .await
        .unwrap();

    if let Some(task) = result {
        Ok(Json(ResponseTask {
            id: task.id,
            title: task.title,
            priority: task.priority,
            description: task.description,
            deleted_at: task.deleted_at,
            user_id: task.user_id,
        }))
    } else {
        Err(StatusCode::NOT_FOUND)
    }
}

#[derive(Deserialize)]
pub struct GetQueryParams {
    priority: Option<String>,
    show_deleted: Option<bool>,
}

pub async fn get_all(
    State(db): State<DatabaseConnection>,
    Query(query_params): Query<GetQueryParams>,
) -> Result<Json<Vec<ResponseTask>>, StatusCode> {
    let mut query_filter = Condition::all();

    if let Some(priority) = query_params.priority {
        query_filter = if priority.is_empty() {
            query_filter.add(tasks::Column::Priority.is_null())
        } else {
            query_filter.add(tasks::Column::Priority.eq(priority))
        };
    }

    if Some(true) != query_params.show_deleted {
        query_filter = query_filter.add(tasks::Column::DeletedAt.is_null());
    }

    let tasks = Tasks::find()
        .filter(query_filter)
        .all(&db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?
        .into_iter()
        .map(|task| ResponseTask {
            id: task.id,
            title: task.title,
            priority: task.priority,
            description: task.description,
            deleted_at: task.deleted_at,
            user_id: task.user_id,
        })
        .collect::<Vec<ResponseTask>>();

    Ok(Json(tasks))
}

#[derive(Deserialize)]
pub struct PutTask {
    priority: Option<String>,
    title: String,
    completed_at: Option<DateTimeWithTimeZone>,
    description: Option<String>,
    deleted_at: Option<DateTimeWithTimeZone>,
    user_id: Option<i32>,
    is_default: Option<bool>,
}

pub async fn atomic_update(
    State(db): State<DatabaseConnection>,
    Path(id): Path<i32>,
    Json(put_task): Json<PutTask>,
) -> Result<(), StatusCode> {
    let update_task = tasks::ActiveModel {
        id: Set(id),
        priority: Set(put_task.priority),
        title: Set(put_task.title),
        completed_at: Set(put_task.completed_at),
        description: Set(put_task.description),
        deleted_at: Set(put_task.deleted_at),
        user_id: Set(put_task.user_id),
        is_default: Set(put_task.is_default),
    };

    Tasks::update(update_task)
        .filter(tasks::Column::Id.eq(id))
        .exec(&db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(())
}
