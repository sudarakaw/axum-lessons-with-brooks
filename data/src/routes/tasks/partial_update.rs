use axum::{
    extract::{Path, State},
    http::StatusCode,
    Json,
};
use sea_orm::{
    prelude::DateTimeWithTimeZone, ColumnTrait, DatabaseConnection, EntityTrait, IntoActiveModel,
    QueryFilter, Set,
};
use serde::Deserialize;

use crate::database::{prelude::Tasks, tasks};

#[derive(Deserialize)]
pub struct PartialTask {
    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    priority: Option<Option<String>>,

    title: Option<String>,

    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    completed_at: Option<Option<DateTimeWithTimeZone>>,

    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    description: Option<Option<String>>,

    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    deleted_at: Option<Option<DateTimeWithTimeZone>>,

    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    is_default: Option<Option<bool>>,
}

pub async fn partial_update(
    State(db): State<DatabaseConnection>,
    Path(id): Path<i32>,
    Json(partial_task): Json<PartialTask>,
) -> Result<(), StatusCode> {
    let result = Tasks::find_by_id(id)
        .one(&db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    let mut task = if let Some(task) = result {
        task.into_active_model()
    } else {
        return Err(StatusCode::NOT_FOUND);
    };

    if let Some(priority) = partial_task.priority {
        task.priority = Set(priority);
    }

    if let Some(title) = partial_task.title {
        task.title = Set(title);
    }

    if let Some(completed_at) = partial_task.completed_at {
        task.completed_at = Set(completed_at);
    }

    if let Some(description) = partial_task.description {
        task.description = Set(description);
    }

    if let Some(deleted_at) = partial_task.deleted_at {
        task.deleted_at = Set(deleted_at);
    }

    if let Some(is_default) = partial_task.is_default {
        task.is_default = Set(is_default);
    }

    Tasks::update(task)
        .filter(tasks::Column::Id.eq(id))
        .exec(&db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(())
}
