use axum::{
    extract::{Path, Query, State},
    http::StatusCode,
};
use sea_orm::{
    ActiveValue::Set, ColumnTrait, DatabaseConnection, EntityTrait, IntoActiveModel, QueryFilter,
};
use serde::Deserialize;

use crate::database::{prelude::Tasks, tasks};

#[derive(Deserialize)]
pub struct QueryParam {
    hard: Option<bool>,
}

pub async fn delete(
    State(db): State<DatabaseConnection>,
    Path(id): Path<i32>,
    Query(query_param): Query<QueryParam>,
) -> Result<(), StatusCode> {
    if Some(true) == query_param.hard {
        hard_delete(&db, id).await
    } else {
        soft_delete(&db, id).await
    }
}

async fn soft_delete(db: &DatabaseConnection, id: i32) -> Result<(), StatusCode> {
    let result = Tasks::find_by_id(id)
        .one(db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    let mut task = if let Some(task) = result {
        task.into_active_model()
    } else {
        return Err(StatusCode::NOT_FOUND);
    };

    let now = chrono::Utc::now();

    task.deleted_at = Set(Some(now.into()));

    Tasks::update(task)
        .filter(tasks::Column::Id.eq(id))
        .exec(db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(())
}

async fn hard_delete(db: &DatabaseConnection, id: i32) -> Result<(), StatusCode> {
    // Check for the record and return 404 if not found before attempting to
    // delete it.
    // let result = Tasks::find_by_id(id)
    //     .one(db)
    //     .await
    //     .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;
    //
    // let task = if let Some(task) = result {
    //     task.into_active_model()
    // } else {
    //     return Err(StatusCode::NOT_FOUND);
    // };
    //
    // Tasks::delete(task)
    //     .exec(db)
    //     .await
    //     .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    // Not checking if the record exists. Attempt to delete non-existing record
    // will return HTTP 200.
    Tasks::delete_by_id(id)
        .exec(db)
        .await
        .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    // Above can also be done like this
    // IMPORTANT: DO NOT FORGET THE `filter`.
    // Tasks::delete_many()
    //     .filter(tasks::Column::Id.eq(id))
    //     .exec(db)
    //     .await
    //     .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

    Ok(())
}
