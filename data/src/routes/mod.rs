mod tasks;
mod users;
mod validate;
mod validate_custom;

use axum::{
    extract::FromRef,
    middleware,
    routing::{delete, get, patch, post, put},
    Router,
};
use sea_orm::DatabaseConnection;

use crate::middleware::authentication;

#[derive(Clone, FromRef)]
pub struct AppState {
    database: DatabaseConnection,
}

pub async fn create_routes(database: DatabaseConnection) -> Router {
    let app_state = AppState { database };

    Router::new()
        .route("/tasks", post(tasks::create))
        .route("/tasks", get(tasks::get_all))
        .route("/tasks/:id", get(tasks::get_one))
        .route("/tasks/:id", put(tasks::atomic_update))
        .route("/tasks/:id", patch(tasks::partial_update))
        .route("/tasks/:id", delete(tasks::delete))
        .route("/users/logout", post(users::logout))
        .layer(middleware::from_fn_with_state(
            app_state.clone(),
            authentication,
        ))
        .route("/users", post(users::create))
        .route("/users/login", post(users::login))
        .with_state(app_state)
        .route("/validate", post(validate::handler))
        .route("/validate-custom", post(validate_custom::handler))
}
