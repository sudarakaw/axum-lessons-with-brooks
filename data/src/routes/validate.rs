use axum::Json;
use serde::Deserialize;

#[allow(dead_code)]
#[derive(Debug, Deserialize)]
pub struct User {
    username: String,
    password: String,
}

pub async fn handler(Json(user): Json<User>) {
    dbg!(&user);
}
