use axum::{
    extract::State,
    headers::{authorization::Bearer, Authorization},
    http::{Request, StatusCode},
    middleware::Next,
    response::Response,
    TypedHeader,
};
use sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter};

use crate::{
    database::{prelude::Users, users},
    utils::{error::AppError, jwt::is_valid},
};

pub async fn authentication<T>(
    State(db): State<DatabaseConnection>,
    TypedHeader(token): TypedHeader<Authorization<Bearer>>,
    mut request: Request<T>,
    next: Next<T>,
) -> Result<Response, AppError> {
    let token = token.token().to_owned();

    let result = Users::find()
        .filter(users::Column::Token.eq(Some(token.clone())))
        .one(&db)
        .await
        .map_err(|_| AppError::new(StatusCode::INTERNAL_SERVER_ERROR, "Internal server error"))?;

    // Token validation is placed after operations that take noticeable time
    // in order to prevent timing attacks from identifying only the token is
    // invalid.
    is_valid(&token)?;

    let Some(user) = result else { return Err(AppError::new( StatusCode::UNAUTHORIZED, "You are not authorized, please login or create an account.")); };

    request.extensions_mut().insert(user);

    Ok(next.run(request).await)
}
