use std::env;

use data::run;
use dotenvy::dotenv;

#[tokio::main]
async fn main() {
    dotenv().ok();

    let database_uri = env::var("DATABASE_URL").unwrap_or_default();

    run(&database_uri).await;
}
